const express =require('express');
const cors=require('cors');
const app =express();
//puerto
app.set('port',process.env.PORT ? process.env.PORT : "4000");
//middleware
app.use(cors());
app.use(express.json());
//rutas
app.get('/api/user',(req,res)=>res.send('UsersRoutes'));
app.get('/api/note',(req,res)=>res.send('Notas :D'));

module.exports=app;